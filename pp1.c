//Copyright Matthew Ling 2021. GPL3
//
//Generate elements to be included in self-extracting archive ppd1 that recreates enwik9
//See readme for details and compilation process

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef  uint8_t  u8;
typedef uint32_t u32;
typedef   size_t  us;
typedef     void   v;

static inline us fread_fully_u8_stdlib(u8 **dest, us *dest_len, char *path){
	FILE *io;
	us ret;
	io=fopen(path, "rb");
	fseek(io, 0, SEEK_END);
	*dest_len=ftell(io);
	rewind(io);
	*dest=malloc(*dest_len);
	ret=fread(*dest, 1, *dest_len, io);
	fclose(io);
	return ret;
}

static inline us fwrite_u32(u32 n, FILE *f){
	us ret=0;
	u8 tmp, i=0;
	while(i<4){
		tmp=(n>>(i*8))&0xFF;
		ret+=fwrite(&tmp, 1, 1, f);
		++i;
	}
	return ret;
}

//replace a set of strings with smaller/equal length strings in place
//every matching string has to start with the same character, which is not present at the start of m
//Also delimit by \0 regardless of wheteher replacements have been made
static inline us replace_less_set(u8 *data, us data_len, u8 start_chr, char **m, us *m_len, char **r, us *r_len, us cnt){
	us i=0, j=0, k;//i=input loc, j=output loc, k=replacement index
	while(i<data_len){
		while(i<data_len && data[i]!=start_chr){
			data[j]=data[i];
			++i;
			++j;
		}
		++i;
		if(i>=data_len)
			break;
		k=0;
		while(k<cnt){
			if(memcmp(data+i, m[k], m_len[k])==0){//replacement
				memcpy(data+j, r[k], r_len[k]);
				i+=m_len[k];
				j+=r_len[k];
				break;
			}
			++k;
		}
		if(k==cnt){//no replacement
			data[j]=start_chr;
			++j;
		}
	}
	data[j]=0;
	return j;
}

//Convert ascii ipv4 to u32, special casing the invalid ips
static inline u32 e_ip(u8 *data){
	char *special_ips[]={"Amillar", "Conversion script", "Efghij", "LittleDan", "Michael Hardy", "Template namespace initialisation script", "The Anome", "64.26.98.xxx", "62.253.64.xxx", "62.119.30.xxx", "217.126.33.xxx", "213.253.40.xxx", "213.253.39.xxx", "199.228.142.xxx", "195.80.96.xxx", "195.149.37.xxx", "193.203.83.xxx", "130.94.122.xxx", "129.33.49.xxx"};
	int index=0, loc=0;
	u32 val=0;
	while(index<19){
		if(memcmp(data+loc, special_ips[index], strlen(special_ips[index]))==0){
			val=index+3740815558;
			break;
		}
		++index;
	}
	if(index==19){//normal ip
		val=(atoi(data+loc)&0xFF)<<24;
		while(data[loc]!='.')
			++loc;
		++loc;
		val+=(atoi(data+loc)&0xFF)<<16;
		while(data[loc]!='.')
			++loc;
		++loc;
		val+=(atoi(data+loc)&0xFF)<<8;
		while(data[loc]!='.')
			++loc;
		++loc;
		val+=(atoi(data+loc)&0xFF);
	}
	return val-(67284490/*min ip value*/);
}

//convert ascii timestamp to u32
static inline u32 e_timestamp(u8 *data){
	u8 offsets[6]={3, 5, 8, 11, 14, 17};
	u8 shifts[]={26, 22, 17, 12, 6, 0};
	u8 reductions[]={2, 1, 1, 0, 0, 0};
	u32 val=0;
	for(int k=0;k<6;++k)
		val+=((atoi(data+offsets[k])-reductions[k])<<shifts[k]);
	return val;
}

//Parse enwik9 into buckets and also do some basic processing
static inline v e1splitter(u8 *data, us data_len){
	u32 unique_id[19506];
	int unique_id_cnt=0, unique_id_index;
	char *special_id[]={"Hajor", "Jake Nelson", "Larry_Sanger", "Mark Ryan"};//the only cases where there is not a 1:1 mapping between id and username
	int special_id_index=0;
	us username_dumploc=0, username_start=0;//defer dumping username until the id is read so we know if we need to dump it (dedupe usernames)

	char *split_name[]={"1.pd", "1.rd", "1.id", "1.ip", "1.timestamp", "1.username", "1.comment", "1.text"};
	char *split_tags[]={
		"<pd>", "<rd>", "<id>", "<ip>", "<timestamp>", "<username>", "<comment>", "<text xml:space=\"preserve\">", "<title>",
		"</pd>", "</rd>", "</id>", "</ip>", "</timestamp>", "</username>", "</comment>", "</text>", "</title>", "</page>",
	};
	FILE *io[8];
	u8 delim=0, newline='\n', pd;
	u32 val, prev_pd=0;
	us i=0, start, dumploc=0, newlen;

	char *rep_m[]={"amp;", "quot;", "lt;", "gt;"};
	char *rep_r[]={"&", "\"", "<", ">"};
	us rep_m_len[]={4, 5, 3, 3};
	us rep_r_len[]={1, 1, 1, 1};

	io[0]=fopen("0.head", "wb");
	fwrite(data, 1, 1403, io[0]);
	fclose(io[0]);

	for(int j=0;j<8;++j)
			io[j]=fopen(split_name[j], "wb");

	while(1){//for every tag
		while(i<data_len && data[i]!='<')
			++i;
		if(i==data_len)
			break;
		start=i;
		while(i<data_len && data[i]!='>')
			++i;
		if(i==data_len)
			break;

		for(int j=0;j<19;++j){//see if we care
			if(memcmp(split_tags[j], data+start, ((i-start)+1))==0){
				if(j<9)//opening tag to something we care about
					dumploc=i+1;
				else{//closing tag to something we care about
					switch(j-9){
						case 0://pd: u8 (pd is strictly increasing and the max step is ~190)
							val=atol(data+dumploc);
							pd=(val-prev_pd)&0xFF;
							prev_pd=val;
							fwrite(&pd, 1, 1, io[j-9]);
							break;
						case 1://rd: u32 shifted to 0
							val=atol(data+dumploc)-15898943;
							fwrite_u32(val, io[j-9]);
							break;
						case 2://id
							val=(special_id_index==4)?atol(data+dumploc):special_id_index+1026976;
							unique_id_index=0;
							while(unique_id_index<unique_id_cnt){
								if(unique_id[unique_id_index]==val)
									break;
								++unique_id_index;
							}
							if(unique_id_cnt==unique_id_index){//new username, dump it
								newlen=replace_less_set(data+username_dumploc, username_start-username_dumploc, '&', rep_m, rep_m_len, rep_r, rep_r_len, 4);
								fwrite(data+username_dumploc, 1, newlen, io[5]);
								fwrite(&delim, 1, 1, io[5]);
								unique_id[unique_id_index]=val;
								++unique_id_cnt;
							}
							fwrite_u32(val, io[j-9]);//always dump id
							break;
						case 3://ip: u32
							val=e_ip(data+dumploc);
							fwrite_u32(val, io[j-9]);
							break;
						case 4://timestamp
							val=e_timestamp(data+dumploc);
							fwrite_u32(val, io[j-9]);
							break;
						case 5://username
							special_id_index=0;
							while(special_id_index<4){
								if((start-dumploc==strlen(special_id[special_id_index])) && (memcmp(data+dumploc, special_id[special_id_index], strlen(special_id[special_id_index]))==0))
									break;
								++special_id_index;
							}
							username_dumploc=dumploc;
							username_start=start;
							break;
						case 6://comment
							//& substitution
							newlen=replace_less_set(data+dumploc, start-dumploc, '&', rep_m, rep_m_len, rep_r, rep_r_len, 4);
							fwrite(data+dumploc, 1, newlen, io[j-9]);
							fwrite(&delim, 1, 1, io[j-9]);
							break;
						case 7://text
							//& substitution
							newlen=replace_less_set(data+dumploc, start-dumploc, '&', rep_m, rep_m_len, rep_r, rep_r_len, 4);
							fwrite(data+dumploc, 1, newlen, io[7]);
							break;
						case 8://title
							//& substitution
							newlen=replace_less_set(data+dumploc, start-dumploc, '&', rep_m, rep_m_len, rep_r, rep_r_len, 4);
							fwrite(data+dumploc, 1, newlen, io[7]);
							fwrite(&newline, 1, 1, io[7]);
							break;
						case 9:// end of page
							//to maintain structure in 1.text we want to output a delimiter for <text></text> AND <text />
							//which we can do by adding it at </page> instead of </text>
							fwrite(&delim, 1, 1, io[7]);
							break;
					}
				}
				break;
			}
		}
	}

	for(int j=0;j<8;++j)
		fclose(io[j]);
}

enum xml_flags{xml_restrict=0x01, xml_contrib=0x02, xml_minor=0x04, xml_comment=0x08, xml_text=0x10};

//Generate control file 0.xml
static inline v e0xml(u8 *data, us data_len){
	FILE *io;
	u8 val=0, xml_val_table[16]={8, 12, 0, 10, 2, 4, 14, 9, 13, 11, 6, 3, 1, 5, 18, 27}, xml_val_table_reverse[28];
	us i=0;
	for(int k=0;k<16;++k)
		xml_val_table_reverse[xml_val_table[k]]=k;
	io=fopen("0.xml", "wb");
	while(i<data_len){
		while(data[i]!='<')
			++i;
		if(memcmp(data+i, "<restrictions>", 14)==0){
			val|=xml_restrict;
			i+=(29/*tags*/+5/*content min*/+111/*skip to ip/username*/);
		}
		else if(memcmp(data+i, "<ip>", 4)==0){
			val|=xml_contrib;
			i+=(9/*tags*/+6/*content min*/+27/*skip to text*/);
		}
		else if(memcmp(data+i, "<minor />", 9)==0){
			val|=xml_minor;
			i+=(9/*tags*/+7/*skip to comment/text*/);
		}
		else if(memcmp(data+i, "<comment>", 9)==0){
			val|=xml_comment;
			i+=19;
		}
		else if(memcmp(data+i, "</page>", 7)==0){
			fwrite(xml_val_table_reverse+val, 1, 1, io);
			val=0;
			i+=(13/*tags*/+29/*skip to id*/);
		}
		else if(memcmp(data+i, "<text xml:space=\"preserve\" />", 29)==0){
			val|=xml_text;
			i+=29;
		}
		else
			++i;
	}
	fclose(io);
}

int main(int argc, char *argv[]){
	char *tail="</text></revision></page>";
	char *rep_m[]={"/title>\n    <id>", "revision>\n      <id>", "/id>\n    <", "/id>\n      <timestamp>"};
	char *rep_r[]={"</title>\n    <pd>", "<revision>\n      <rd>", "</pd>\n    <", "</rd>\n      <timestamp>"};
	us rep_m_len[]={16, 20, 10, 22};
	us rep_r_len[]={17, 21, 11, 23};
	u8 *data=NULL;
	us data_len=0;
	//read enwik9 from file
	if(argc==2)
		fread_fully_u8_stdlib(&data, &data_len, argv[1]);
	else
		fread_fully_u8_stdlib(&data, &data_len, "../enwik9");
	//append xml tags to make parsing easier
	data=realloc(data, data_len+strlen(tail)+1);
	strcpy((char*)data+1000000000, tail);
	data_len+=strlen(tail);
	//split <id> into three tags for easier parsing (<id>, <pd>, <rd>)
	replace_less_set(data, data_len, '<', rep_m, rep_m_len, rep_r, rep_r_len, 4);

	e0xml(data, data_len);
	e1splitter(data, data_len);
}
