//Copyright Matthew Ling 2021. GPL3
//
//Preprocesses enwik9 by parsing the XML and storing related tags together
//in \0 delimited buckets. These buckets are used to create a self-extracting
//archive ppd0 that generates enwik9
//0.pd: Page id
//0.rd: Revision id
//0.id: Contributor id
//0.ip: ipv4 addresses with some special cases
//0.timestamp: Timestamps, still \0 delimited despite all having a fixed size
//0.username: Usernames
//0.text: Title, main body and comment body of all pages in order \0 delimited.
//        This contains the bulk of enwik9 untouched
//<restrictions> is fully modelled in ppd0 so there is no 0.restrictions file
//There is one additional bucket, 0.xml. It is a control file that specifies
//the XML tags so they can be regenerated. There are 16 unique page types
//specified in xml_val_table. 0.xml stores the indexes of each page's value in
//this table. More frequent page types have a lower index value

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef uint8_t u8;
typedef size_t us;
typedef void v;

static inline us fread_fully_u8_stdlib(u8 **dest, us *dest_len, char *path){
	FILE *io;
	us ret;
	io=fopen(path, "rb");
	fseek(io, 0, SEEK_END);
	*dest_len=ftell(io);
	rewind(io);
	*dest=malloc(*dest_len);
	ret=fread(*dest, 1, *dest_len, io);
	fclose(io);
	return ret;
}

//WARNING: Sloppy function that only works specifically here
//in the name of being quicker but also lazy
//Replace a set of strings with equal-sized replacements
//All matching strings must start with the same character
static inline v replace_equal_set_enwik9(u8 *data, char **matches, char **replacements, us *lengths, us cnt){
	us i=0, j;
	u8 start_chr=matches[0][0];
	while(i<1000000000){//can be sloppy, oversized from tail
		while(data[i]!=start_chr)
			++i;
		j=0;
		while(j<cnt){
			if(memcmp(data+i, matches[j], lengths[j])==0){
				memcpy(data+i, replacements[j], lengths[j]);
				i+=lengths[j];
				break;
			}
			++j;
		}
		if(j==cnt)
			++i;
	}
}

//Parse all the content into buckets
static inline v e0splitter(u8 *data, us data_len){
	u8 delim=0;
	us i=0, start, dumploc=0;
	FILE *io[9];
	char *split_tags[]={
		"<pd>", "<rd>", "<id>", "<timestamp>", "<username>", "<ip>", "<text xml:space=\"preserve\">", "<comment>", "<title>",
		"</pd>", "</rd>", "</id>", "</timestamp>", "</username>", "</ip>", "</text>", "</comment>", "</title>",
	};
	char *split_name[]={"0.pd", "0.rd", "0.id", "0.timestamp", "0.username", "0.ip", "0.text"};

	io[0]=fopen("0.head", "wb");
	fwrite(data, 1, 1403, io[0]);
	fclose(io[0]);

	for(int j=0;j<7;++j)
		io[j]=fopen(split_name[j], "wb");
	io[7]=io[6];
	io[8]=io[6];

	while(1){//for every tag
		while(i<data_len && data[i]!='<')
			++i;
		if(i==data_len)
			break;
		start=i;
		while(i<data_len && data[i]!='>')
			++i;
		if(i==data_len)
			break;

		for(int j=0;j<18;++j){//see if we care
			if(memcmp(split_tags[j], data+start, ((i-start)+1))==0){
				if(j<9)//new dump
					dumploc=i+1;
				else{//finish dump
					fwrite(data+dumploc, 1, start-dumploc, io[j-9]);
					fwrite(&delim, 1, 1, io[j-9]);
				}
				break;
			}
		}
	}

	for(int j=0;j<7;++j)
		fclose(io[j]);
}

enum xml_flags{xml_restrict=0x01, xml_contrib=0x02, xml_minor=0x04, xml_comment=0x08, xml_text=0x10};

//Generate control file 0.xml
static inline v e0xml(u8 *data, us data_len){
	FILE *io;
	u8 val=0, xml_val_table[16]={8, 12, 0, 10, 2, 4, 14, 9, 13, 11, 6, 3, 1, 5, 18, 27}, xml_val_table_reverse[28];
	us i=0;
	for(int k=0;k<16;++k)
		xml_val_table_reverse[xml_val_table[k]]=k;
	io=fopen("0.xml", "wb");
	while(i<data_len){
		while(data[i]!='<')
			++i;
		if(memcmp(data+i, "<restrictions>", 14)==0){
			val|=xml_restrict;
			i+=29;
		}
		else if(memcmp(data+i, "<ip>", 4)==0){
			val|=xml_contrib;
			i+=9;
		}
		else if(memcmp(data+i, "<minor />", 9)==0){
			val|=xml_minor;
			i+=9;
		}
		else if(memcmp(data+i, "<comment>", 9)==0){
			val|=xml_comment;
			i+=19;
		}
		else if(memcmp(data+i, "</page>", 7)==0){
			fwrite(xml_val_table_reverse+val, 1, 1, io);
			val=0;
			i+=13;
		}
		else if(memcmp(data+i, "<text xml:space=\"preserve\" />", 29)==0){
			val|=xml_text;
			i+=29;
		}
		else
			++i;
	}
	fclose(io);
}

int main(int argc, char *argv[]){
	char *tail="</text></revision></page>";
	char *rep_m[]={"</title>\n    <id>", "<revision>\n      <id>", "</id>\n    <", "</id>\n      <timestamp>"};
	char *rep_r[]={"</title>\n    <pd>", "<revision>\n      <rd>", "</pd>\n    <", "</rd>\n      <timestamp>"};
	us rep_l[]={17, 21, 11, 23};
	u8 *data=NULL;
	us data_len=0;
	//read enwik9 from file
	if(argc==2)
		fread_fully_u8_stdlib(&data, &data_len, argv[1]);
	else
		fread_fully_u8_stdlib(&data, &data_len, "../enwik9");
	//append xml tags to make parsing easier
	data=realloc(data, data_len+strlen(tail)+1);
	strcpy((char*)data+1000000000, tail);
	data_len+=strlen(tail);
	//split <id> into three tags for easier parsing (<id>, <pd>, <rd>)
	replace_equal_set_enwik9(data, rep_m, rep_r, rep_l, 4);

	e0xml(data, data_len);
	e0splitter(data, data_len);
}
