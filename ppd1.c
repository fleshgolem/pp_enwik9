//Copyright Matthew Ling 2021. GPL3
//
//ppd1 is a self-extracting archive that generates enwik9. The archive
//does not perform (much) compression, it acts as a preprocessor so anything
//not modelled is left untouched and available for a compressor to work on.
//See readme for details and compilation process

#include <inttypes.h>
#include <stdio.h>
#include <string.h>

typedef  uint8_t  u8;
typedef uint32_t u32;
typedef   size_t  us;
typedef     void   v;

#define U32 PRIu32

//write string to file excluding delimiter
//delimiter is usually \0 for C-Strings, however titles are delimited by \n
//Also regenerate top level HTML entities
static inline v write_str_sub(const char *data, FILE *io, u8 delim){
	char *rep_m[]={"&amp;", "&quot;", "&lt;", "&gt;"};
	const char *ptr=data;
	us rep_m_len[]={5, 6, 4, 4};
	while(*ptr!=delim){
		switch(*ptr){
			case '&':
				fwrite(rep_m[0], 1, rep_m_len[0], io);
				break;
			case '\"':
				fwrite(rep_m[1], 1, rep_m_len[1], io);
				break;
			case '<':
				fwrite(rep_m[2], 1, rep_m_len[2], io);
				break;
			case '>':
				fwrite(rep_m[3], 1, rep_m_len[3], io);
				break;
			default:
				fwrite(ptr, 1, 1, io);
				break;
		}
		++ptr;
	}
}

//go to next element in string array with given delimiter
//delimiter is usually \0 for C-Strings, however titles are delimited by \n
static inline v goto_next_element(const char **data, u8 delim){
	while(**data!=delim)
		++*data;
	++*data;
}

//bitstore is very basic varint encoding. 0=0, 1=10, 2=110, 3=1110, ...
struct bitstore{
	u8 *bits;
	us loc;
};
static inline int bitstore_get(struct bitstore *bs){
	int ret=0;
	while( ((bs->bits[bs->loc/8]>>(bs->loc%8))&0x01)==1){
		++bs->loc;
		++ret;
	}
	++bs->loc;
	return ret;
}

//map ids to usernames, used to reverse username dedupe
struct map_id_username{
	u32 id;
	u8 username[60];//maximum username length is 58
};

//convert ips back to text
static inline v d_ip(u32 val, u8 *data){
	char *special_ips[]={"Amillar", "Conversion script", "Efghij", "LittleDan", "Michael Hardy", "Template namespace initialisation script", "The Anome", "64.26.98.xxx", "62.253.64.xxx", "62.119.30.xxx", "217.126.33.xxx", "213.253.40.xxx", "213.253.39.xxx", "199.228.142.xxx", "195.80.96.xxx", "195.149.37.xxx", "193.203.83.xxx", "130.94.122.xxx", "129.33.49.xxx"};
	if(val>=3740815558)
		strcpy(data, special_ips[val-3740815558]);
	else
		sprintf(data, "%u.%u.%u.%u", (val>>24)&0xFF, (val>>16)&0xFF, (val>>8)&0xFF, (val>>0)&0xFF);
}

//convert timestamps back to text
static inline v d_timestamp(u32 val, u8 *data){
	sprintf(data, "200%d-%02d-%02dT%02d:%02d:%02dZ", ((val>>26)&0x07)+2, ((val>>22)&0x0F)+1, ((val>>17)&0x1F)+1, ((val>>12)&0x1F), ((val>>6)&0x3F), ((val>>0)&0x3F));
}

enum xml_flags{xml_restrict=0x01, xml_contrib=0x02, xml_minor=0x04, xml_comment=0x08, xml_text=0x10};

int main(int argc, char *argv[]){
	FILE *io;
	us i=0;
	u8 buf[64];
	u32 buf32, prev_pd=0;

	//embed all the sections using asm
	extern const char __start_xmlhead[];
	extern const char __start_xmltext[];
	extern const char __start_xmlcomment[];
	extern const char __start_xmlusername[];
	extern const char __start_xml[];
	extern const char __start_xmlpd[];
	extern const u8 __start_xmlrd[];
	extern const u8 __start_xmlid[];
	extern const u8 __start_xmltimestamp[];
	extern const u32 __start_xmlip[];
	__asm__("\n\
	.pushsection xmlhead, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"0.head\"\n\
	.popsection\n"\
	"\n\
	.pushsection xmltext, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"1.text\"\n\
	.popsection\n"\
	"\n\
	.pushsection xmlcomment, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"1.comment\"\n\
	.popsection\n"\
	"\n\
	.pushsection xmlusername, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"1.username\"\n\
	.popsection\n"\
	"\n\
	.pushsection xml, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"0.xml\"\n\
	.popsection\n"\
	"\n\
	.pushsection xmlpd, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"1.pd\"\n\
	.popsection\n"\
	"\n\
	.pushsection xmlrd, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"1.rd\"\n\
	.popsection\n"\
	"\n\
	.pushsection xmlid, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"1.id\"\n\
	.popsection\n"\
	"\n\
	.pushsection xmltimestamp, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"1.timestamp\"\n\
	.popsection\n"\
	"\n\
	.pushsection xmlip, \"a\", @progbits\n\
	.align 8\n\
	.incbin \"1.ip\"\n\
	.popsection\n"\
	);
	const char *xmlhead=__start_xmlhead;
	const char *xmltext=__start_xmltext;
	const char *xmlcomment=__start_xmlcomment;
	const char *xmlusername=__start_xmlusername;
	const char *xml=__start_xml;
	const u8 *xmlpd=(u8*)__start_xmlpd;
	const u32 *xmlrd=(u32*)__start_xmlrd;
	const u32 *xmlid=(u32*)__start_xmlid;
	const u32 *xmltimestamp=(u32*)__start_xmltimestamp;
	const u32 *xmlip=(u32*)__start_xmlip;

	//username deduplication
	struct map_id_username unique_id[19506]={0};
	int unique_id_cnt=0, unique_id_index;
	int special_id[]={23076, 16897, 216, 176};


	//xml modelling
	char *xml_data[]={
	"\n  <page>\n    <title>",
	"</title>\n    <id>",
	"</id>\n    ",
	"<restrictions>",
	"</restrictions>\n    ",
	"<revision>\n      <id>",
	"</id>\n      <timestamp>",
	"</timestamp>\n      <contributor>\n        ",
	"<ip>",
	"</ip>\n      </contributor>\n      ",
	"<username>",
	"</username>\n        <id>",
	"</id>\n      </contributor>\n      ",
	"<minor />\n      ",
	"<comment>",
	"</comment>\n      ",
	"<text xml:space=\"preserve\" />",
	"<text xml:space=\"preserve\">",
	"</text>",
	"\n    </revision>\n  </page>",
	};
	u8 xml_len[]={21, 17, 10, 14, 20, 21, 23, 41, 4, 33, 10, 24, 33, 16, 9, 17, 29, 27, 7, 26};
	u8 xml_val_table[16]={8, 12, 0, 10, 2, 4, 14, 9, 13, 11, 6, 3, 1, 5, 18, 27};

	//<restrictions> modelling
	//There are only 8 possible strings stored in <restrictions>, and they're heavily skewed.
	//<restrictions> contents is stored here as a bitstore
	u8 restrict_data[178]={119, 130, 0, 32, 7, 0, 0, 0, 63, 0, 240, 0, 205, 11, 0, 128, 7, 239, 133, 60, 60, 1, 184, 0, 0, 62, 8, 240, 128, 7, 128, 227, 0, 0, 64, 64, 79, 121, 240, 0, 128, 3, 47, 48, 192, 1, 0, 0, 224, 7, 8, 0, 120, 0, 192, 3, 28, 128, 39, 112, 176, 0, 92, 0, 4, 8, 7, 248, 130, 30, 0, 31, 120, 78, 192, 67, 56, 15, 0, 0, 0, 124, 56, 0, 128, 48, 31, 15, 112, 0, 128, 15, 0, 1, 0, 14, 237, 228, 0, 0, 128, 3, 192, 7, 0, 0, 216, 16, 0, 0, 14, 24, 60, 190, 1, 224, 195, 128, 3, 225, 0, 6, 60, 0, 193, 108, 64, 0, 27, 31, 112, 240, 108, 240, 225, 32, 28, 240, 124, 48, 0, 6, 0, 0, 135, 15, 15, 176, 253, 177, 109, 124, 32, 0, 0, 54, 12, 0, 60, 0, 111, 96, 192, 65, 192, 1, 128, 29, 3, 62, 124, 128, 0, 86, 0, 0, 0, 7};
	u8 restrict_len[]={11, 10, 5, 21, 37, 21, 18, 29};
	char *restrict_strings[]={
	"move=:edit=",//862 instances
	"move=sysop",//39
	"sysop",//34
	"edit=sysop:move=sysop",//31
	"edit=autoconfirmed:move=autoconfirmed",//28
	"move=sysop:edit=sysop",//16
	"move=autoconfirmed",//2
	"edit=autoconfirmed:move=sysop",//1
	};
	int restrict_index;
	struct bitstore bs;

	bs.bits=restrict_data;
	bs.loc=0;
	io=fopen((argc==2)?argv[1]:"enwik9", "wb");

	fwrite(xmlhead, 1, 1403, io);
	while(1){//for every page
		fwrite(xml_data[0], 1, xml_len[0], io);
		write_str_sub(xmltext, io, '\n');//title
		goto_next_element(&xmltext, '\n');
		fwrite(xml_data[1], 1, xml_len[1], io);
		prev_pd+=*xmlpd;
		++xmlpd;
		sprintf(buf, "%"U32, prev_pd);
		fwrite(buf, 1, strlen(buf), io);//pd
		fwrite(xml_data[2], 1, xml_len[2], io);
		if(xml_val_table[xml[i]]&xml_restrict){
			fwrite(xml_data[3], 1, xml_len[3], io);
			restrict_index=bitstore_get(&bs);
			fwrite(restrict_strings[restrict_index], 1, restrict_len[restrict_index], io);//restrict
			fwrite(xml_data[4], 1, xml_len[4], io);
		}
		fwrite(xml_data[5], 1, xml_len[5], io);
		buf32=*xmlrd+15898943;
		++xmlrd;
		sprintf(buf, "%"U32, buf32);
		fwrite(buf, 1, strlen(buf), io);//rd
		fwrite(xml_data[6], 1, xml_len[6], io);
		d_timestamp(*xmltimestamp, buf);
		++xmltimestamp;
		fwrite(buf, 1, strlen(buf), io);//timestamp
		fwrite(xml_data[7], 1, xml_len[7], io);
		if(xml_val_table[xml[i]]&xml_contrib){//all pages have either an ip or username+id as a contributor
			fwrite(xml_data[8], 1, xml_len[8], io);
			d_ip(*xmlip+(67284490/*min ip value*/), buf);
			++xmlip;
			fwrite(buf, 1, strlen(buf), io);//ip
			fwrite(xml_data[9], 1, xml_len[9], io);
		}
		else{//username+id
			//undo username dedupe
			unique_id_index=0;
			while(unique_id_index<unique_id_cnt){
				if(unique_id[unique_id_index].id==*xmlid)
					break;
				++unique_id_index;
			}
			if(unique_id_cnt==unique_id_index){//new username, add to db
				memset(unique_id+unique_id_index, 0, sizeof(struct map_id_username));
				unique_id[unique_id_index].id=*xmlid;
				strcpy(unique_id[unique_id_index].username, xmlusername);
				goto_next_element(&xmlusername, '\0');
				++unique_id_cnt;
			}
			fwrite(xml_data[10], 1, xml_len[10], io);
			write_str_sub(unique_id[unique_id_index].username, io, '\0');//username
			fwrite(xml_data[11], 1, xml_len[11], io);
			if(*xmlid<1026976)
				sprintf(buf, "%"U32, *xmlid);
			else
				sprintf(buf, "%"U32, special_id[*xmlid-1026976]);//undo special id
			++xmlid;
			fwrite(buf, 1, strlen(buf), io);//id
			fwrite(xml_data[12], 1, xml_len[12], io);
		}
		if(xml_val_table[xml[i]]&xml_minor)
			fwrite(xml_data[13], 1, xml_len[13], io);
		if(xml_val_table[xml[i]]&xml_comment){
			fwrite(xml_data[14], 1, xml_len[14], io);
			write_str_sub(xmlcomment, io, '\0');//comment
			goto_next_element(&xmlcomment, '\0');
			fwrite(xml_data[15], 1, xml_len[15], io);
		}
		if(xml_val_table[xml[i]]&xml_text){//empty text
			fwrite(xml_data[16], 1, xml_len[16], io);
			++xmltext;//skip unused delimiter
		}
		else{//text
			fwrite(xml_data[17], 1, xml_len[17], io);
			write_str_sub(xmltext, io, '\0');//main body
			goto_next_element(&xmltext, '\0');
			if(i==243425)//enwik9 ends in the middle of this text section
				break;
			fwrite(xml_data[18], 1, xml_len[18], io);
		}
		//end revision end page
		fwrite(xml_data[19], 1, xml_len[19], io);
		++i;
	}
	fclose(io);
	return 0;
}
